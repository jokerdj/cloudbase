## Cloudbase for Flutter support null safety

升级cloudbase库支持null safety

## Cloudbase  for Flutter


[腾讯云·云开发](https://www.cloudbase.net/)的 Flutter 插件，更多的云开发 Flutter 插件请见[云开发文档](https://docs.cloudbase.net/api-reference/flutter/install.html)。

## 安装

在 flutter 项目的 `pubspec.yaml` 文件的 `dependencies` 中添加

```yaml
dependencies:
  cloudbase_core: #0.0.12
    git:
      url: https://gitee.com/jokerdj/cloudbase.git
      path: cloudbase_core
  cloudbase_auth: #0.0.14
    git:
      url: https://gitee.com/jokerdj/cloudbase.git
      path: cloudbase_auth
  cloudbase_function: #0.0.2
    git:
      url: https://gitee.com/jokerdj/cloudbase.git
      path: cloudbase_function
  cloudbase_storage: #0.0.3
    git:
      url: https://gitee.com/jokerdj/cloudbase.git
      path: cloudbase_storage
  cloudbase_database: #0.0.12+2
    git:
      url: https://gitee.com/jokerdj/cloudbase.git
      path: cloudbase_database
```
