import 'dart:convert';

class CloudBaseStorageRes<T> {
  String requestId;
  T? data;

  CloudBaseStorageRes({required this.requestId, this.data});

  @override
  String toString() {
    return jsonEncode({'requestId': this.requestId, 'data': data});
  }
}

class UploadRes {
  late String fileId;

  UploadRes({required this.fileId});

  UploadRes.fromMap(Map<String, dynamic> map) {
    this.fileId = map['fileId'];
  }
}

class UploadMetadata {
  late String url;

  late String token;

  late String authorization;

  late String fileId;

  late String cosFileId;

  UploadMetadata({
    required this.url,
    required this.token,
    required this.authorization,
    required this.fileId,
    required this.cosFileId,
  });

  UploadMetadata.fromMap(Map<String, dynamic> map) {
    this.url = map['url'];
    this.token = map['token'];
    this.authorization = map['authorization'];
    this.fileId = map['fileId'];
    this.cosFileId = map['cosFileId'];
  }

  @override
  String toString() {
    return jsonEncode({
      'url': url,
      'token': token,
      'authorization': authorization,
      'fileId': fileId,
      'cosFileId': cosFileId
    });
  }

  toJson() {
    return jsonEncode({
      'url': url,
      'token': token,
      'authorization': authorization,
      'fileId': fileId,
      'cosFileId': cosFileId
    });
  }
}

class DownloadMetadata {
  String? fileId;
  String? downloadUrl;

  DownloadMetadata.fromMap(Map map) {
    fileId = map['fileid'];
    downloadUrl = map['download_url'];
  }

  @override
  String toString() {
    return jsonEncode({'fileId': fileId, 'downloadUrl': downloadUrl});
  }

  toJson() {
    return jsonEncode({'fileId': fileId, 'downloadUrl': downloadUrl});
  }
}

class DeleteMetadata {
  late String fileId;
  // code 为 'SUCCESS' 表示删除成功
  late String code;

  DeleteMetadata.fromMap(Map map) {
    fileId = map['fileid'];
    code = map['code'];
  }

  @override
  String toString() {
    return jsonEncode({'fileId': fileId, 'code': code});
  }

  toJson() {
    return jsonEncode({'fileId': fileId, 'code': code});
  }
}
