class ServerDate {
  num offset;

  ServerDate({this.offset = 0});

  Map toJson() {
    return {
      '\$date': {'offset': offset}
    };
  }
}
