import 'package:cloudbase_database/src/realtime/snapshot.dart';

class RealtimeListener {
  Function close;
  late Function onChange;
  late Function onError; // todo 格式化

  RealtimeListener(
      {required this.close, Function? onChange, Function? onError}) {
    if (onChange == null) {
      this.onChange = (Snapshot snapshot) {};
    }
    if (onError == null) {
      this.onError = (error) {};
    }
  }
}
